# My Engineering Logbook 

![UPM-Logo-Vector](/uploads/b0fe5cc0422a4d5261e7ae794574dc2f/UPM-Logo-Vector.png)
## Integrated Design Project Log 


|Nama: Najwa Husna Binti Amirruddin|Matric.No:199131|Subsystem: Simulation|
|------|------|------|

|Logbook Entry: 01| Date: 4/11/2021|
|------|------|

|1. Our Agenda|
|------------|
|a) We Formed up a ganttchart to show our progress of our project for this subject.|
|b) Discuss the timeline of the progress in simulations (Real Time Measurements observing, data intake, Catia Programed Drawing or Integrated Designing, Performance Calculations of the HAU, Computational Fluid Dynamics Analysis (CFD), Ansys.|

|2. Our Goals|
|------------|
|a) Looking the design of HAU in real time.|
|b) Measurements intake from the modelled exact HAU.|
|c) Selections of the measurements in the Top, side, bottom view.|
|d) Overall complete design measurements of physical HAU.|

|3. Decision to solve the problem|
|-------|
|a) Took a visit to Lab H 2.1 to look at the HAU model and the real time design.|

|4. Method to solve the problem|
|-----------|
|a) Take the real measurements of the HAU which is available.|
|b) Measurements: Tolerance was set according to the measurements to avoid greater significant on the error involved.|

|5. Justification when solving the problem|
|------------|
|a) To perform a prototype which looks good in terms of the measurements ratio set up in the CATIA programme.|
|b) To perform the good performance calculations or reviews on HAU model.|
|c) To have a good results on the Fluid Dynamics concept.|

|6. Impact of/after the decision chosen|
|-------------------|
|a) Have a good illustrations or imaginations on how to design the HAU in the CATIA (Airframe Design).|
|b) Think of the possibilities parameters involved (Lift, Drag, Thrust, Weight).|
|c) Possible CFD outcomes is accurate.|

|7. Our next step|
|----------|
|a) Designing process will begin in CATIA programme.|
|b) Have to review on the parameters involved from other subsystem group to have a better idea in performance calculations.|

